/**
 * Recebe uma string de data no formato: diasemana, dia Mes Ano hora:minuto:segundo +0000
 * 
 * @param string date - Data no formato: 'Thu, 29 Aug 2019 11:27:01 +0000'.
 * @return string - Data no formato 'Quinta, 29 de Agosto de 2019'.
 */
 
function dataformatada(sData) {
    meses = {
        'Jan' : 'Janeiro',
        'Feb' : 'Fevereiro',
        'Mar' : 'Março',
        'Apr' : 'Abril',
        'May' : 'Maio',
        'Jun' : 'Junho',
        'Jul' : 'Julho',
        'Aug' : 'Agosto',
        'Sep' : 'Setembro',
        'Oct' : 'Outubro',
        'Nov' : 'Novembro',
        'Dec' : 'Dezembro'
    }
    dias = { 'Sun': 'Domingo', 'Mon': 'Segunda', 'Tue': 'Terça', 'Wed': 'Quarta', 'Thu': 'Quinta', 'Fri': 'Sexta', 'Sat': 'Sabado' };
    vet = sData.split(/[\s,]+/);
    return dias[vet[0]] +', '+ vet[1] +' de '+ meses[vet[2]] +' de '+ vet[3]
}